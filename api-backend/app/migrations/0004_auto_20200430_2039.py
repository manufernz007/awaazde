# Generated by Django 3.0.5 on 2020-04-30 15:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20200430_2032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='image',
            field=models.FileField(null=True, upload_to=''),
        ),
    ]
