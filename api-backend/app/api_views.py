from rest_framework.generics import ListAPIView,CreateAPIView,DestroyAPIView,UpdateAPIView,RetrieveAPIView
from app.serializers import ProjectSerializer,TaskSerializer,UserSerializer
from app.models import Project,Task
from rest_framework.filters import SearchFilter
from rest_framework.pagination import LimitOffsetPagination
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import authentication_classes,permission_classes
from rest_framework.permissions import AllowAny
from django.http import Http404
from rest_framework.views import APIView
from django.shortcuts import render
from django.contrib.auth.models import User


class Pagination(LimitOffsetPagination):
    default_limit=5
    max_limit=100

@permission_classes((AllowAny,))
class UserList(ListAPIView):
    queryset=User.objects.all()
    serializer_class=UserSerializer
    pagination_class=Pagination
    filter_backends=(DjangoFilterBackend,SearchFilter)
    filter_fields=('id',)
    search_fields=('name','description',)


@permission_classes((AllowAny,))
class ProjectList(ListAPIView):
    queryset=Project.objects.all()
    serializer_class=ProjectSerializer
    pagination_class=Pagination
    filter_backends=(DjangoFilterBackend,SearchFilter)
    filter_fields=('id',)
    search_fields=('name','description',)


@permission_classes((AllowAny,))
class ProjectCreate(CreateAPIView):
    serializer_class=ProjectSerializer
    def create(self,request,*args,**kwargs):
        name=request.data.get('name')
        return super().create(request,*args,**kwargs)

@permission_classes((AllowAny,))
class ProjectUpdate(UpdateAPIView):
    serializer_class = ProjectSerializer
    lookup_field='id'
    queryset=Project.objects.all()
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs) 


@permission_classes((AllowAny,))     
class ProjectDestroy(DestroyAPIView):
    queryset=Project.objects.all()
    lookup_field='id'

    def delete(self,request,*args,**kwargs):
        project_id=request.data.get('id')
        response=super().delete(request,*args,**kwargs)
        if response.status_code==204:
            from django.core.cache import cache
            cache.delete('project_data_{}'.format(project_id))
        return response

@permission_classes((AllowAny,))
class TaskList(ListAPIView):
    queryset=Task.objects.all()
    serializer_class=TaskSerializer
    pagination_class=Pagination
    filter_backends=(DjangoFilterBackend,SearchFilter)
    filter_fields=('id',)
    search_fields=('name','description',)


@permission_classes((AllowAny,))
class TaskCreate(CreateAPIView):
    serializer_class=TaskSerializer
    def create(self,request,*args,**kwargs):
        name=request.data.get('name')
        return super().create(request,*args,**kwargs)


@permission_classes((AllowAny,))
class TaskUpdate(UpdateAPIView):
    serializer_class = TaskSerializer
    lookup_field='id'
    queryset=Task.objects.all()
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs) 


@permission_classes((AllowAny,))     
class TaskDestroy(DestroyAPIView):
    queryset=Task.objects.all()
    lookup_field='id'

    def delete(self,request,*args,**kwargs):
        task_id=request.data.get('id')
        response=super().delete(request,*args,**kwargs)
        if response.status_code==204:
            from django.core.cache import cache
            cache.delete('task_data_{}'.format(task_id))
        return response

@permission_classes((AllowAny,)) 
class ProjectDetail(RetrieveAPIView):
     def get_object(self, pk):
        try:
            return Project.objects.get(pk=pk)
        except Project.DoesNotExist:
            raise Http404    

@permission_classes((AllowAny,)) 
class TaskDetail(RetrieveAPIView):
     def get_object(self, pk):
        try:
            return Task.objects.get(pk=pk)
        except Task.DoesNotExist:
            raise Http404


