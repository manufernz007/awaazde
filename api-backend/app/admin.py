from django.contrib import admin
from .models import Project,Task


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ['name','duration','task_count']

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ['name','project','parent_task','user','start_date','end_date']
    
    
