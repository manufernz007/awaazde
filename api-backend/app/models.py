from django.db import models
from django.conf import settings

class Project(models.Model):
    name=models.CharField(max_length=100,verbose_name="Project Name")
    description=models.TextField(null=True,verbose_name="Project Description")
    duration=models.IntegerField(verbose_name="Project Duration (in days)")
    image=models.ImageField( verbose_name="Add an avatar",null=True)
    task_count = lambda self: self.task_set.count()
    task_count.short_description="Tasks" 

    def __str__(self):
        return self.name

      

class Task(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE,verbose_name="Project Name")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,verbose_name="Assigned User")
    parent_task=models.ForeignKey('self', on_delete=models.CASCADE,verbose_name="Parent Task", null=True, blank=True)
    name=models.CharField(max_length=100,verbose_name="Task Name")
    description=models.TextField(null=True,verbose_name="Task Description")
    start_date=models.DateField(verbose_name="Start Date")
    end_date=models.DateField(verbose_name="End Date")
    
    def __str__(self):
        return self.name

    

