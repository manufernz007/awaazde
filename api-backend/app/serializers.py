from rest_framework import serializers
from app.models import Project,Task
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=('id','username')

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model=Project
        fields=('id','name','description','duration','image','task_count')

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model=Task
        fields=('id','project','user','parent_task','name','description','start_date','end_date')

