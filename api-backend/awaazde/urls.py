from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.views.static import serve
from django.conf import settings
import app.views
import app.api_views
from django.views.generic.base import TemplateView
from rest_framework.authtoken.views import obtain_auth_token
from django.conf.urls.static import static

urlpatterns = [
    #API Routes for Project
    path('api/projects/',app.api_views.ProjectList.as_view()),
    path('api/project/new',app.api_views.ProjectCreate.as_view()),
    path('api/project/<int:id>/update',app.api_views.ProjectUpdate.as_view()),
    path('api/project/<int:id>/destroy',app.api_views.ProjectDestroy.as_view()),

    #API Routes for Tasks
    path('api/tasks/',app.api_views.TaskList.as_view()),
    path('api/task/new',app.api_views.TaskCreate.as_view()),
    path('api/task/<int:id>/update',app.api_views.TaskUpdate.as_view()),
    path('api/task/<int:id>/destroy',app.api_views.TaskDestroy.as_view()),

    #API Routes for Users
    path('api/users/',app.api_views.UserList.as_view()),

    #Authentication token route
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'), 
    
    #Django Admin Route
    path('', admin.site.urls),
    
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#Few Admin Customisations
admin.site.site_header = "Awaaz.De Projects-Tasks"
admin.site.site_title = "Projects-Tasks"
admin.site.index_title = "Awaaz.De Admin Panel"