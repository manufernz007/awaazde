# PROJECT/TASK APPLICATION#

Application for adding Projects and Tasks to each project


### What is features of the Application?

* Project creation
* Tasks associated with each project
* User assigned to each task

## Code is divided into 2 parts

1.  Frontend folder

    Hosted on ec2 instance: ec2-13-235-113-6.ap-south-1.compute.amazonaws.com

    Built using Angular 9 and Bootstrap 4 with the following dependencies:


    "@angular/animations": "^9.1.4",
    "@angular/common": "~9.1.4",
    "@angular/compiler": "~9.1.4",
    "@angular/core": "~9.1.4",
    "@angular/forms": "~9.1.4",
    "@angular/platform-browser": "~9.1.4",
    "@angular/platform-browser-dynamic": "~9.1.4",
    "@angular/router": "~9.1.4",
    "bootstrap": "^4.4.1",
    "jquery": "^3.5.0",
    "ng2-file-upload": "^1.4.0",
    "ngx-toastr": "^12.0.1",
    "popper.js": "^1.16.1",
    "rxjs": "~6.5.4",
    "tslib": "^1.10.0",
    "zone.js": "~0.10.2"


2.  Backend Folder

    Hosted on ec2 instance: ec2-13-233-140-28.ap-south-1.compute.amazonaws.com

    Built using Django Rest Framework version 3.11.0

    Python version: 3.8.2

    The admin can also be views using django-admin:

    ec2-13-233-140-28.ap-south-1.compute.amazonaws.com

    superusername: admin

    password: admin



### API's Routes Explained

BASE URI: ec2-13-233-140-28.ap-south-1.compute.amazonaws.com/


# API Routes for Projects

*   'api/projects/' => Gets list of all projects (GET)

*   'api/project/new' => Creates new project (POST)

*   'api/project/<int:id>/update' => Updates existing project (PUT)

*   'api/project/<int:id>/destroy' => Delete particular project (DELETE)


# API Routes for Tasks

*   'api/tasks/' => Gets list of all Tasks (GET)

*   'api/task/new' => Creates new Task (POST)

*   'api/task/<int:id>/update' => Updates existing Task (PUT)

*   'api/task/<int:id>/destroy' => Delete particular Task (DELETE)


# API Routes for Users

*   'api/users/' => Gets list of all Users (GET)


# Authentication token route

*   'api-token-auth/' => Gets user token on login which could be used for API Authentication (POST) 


### Note that TokenAuthentication is added to the code however all the API's have the @permission_classes((AllowAny,)) to bypass the authentication as it requires SSL to be enabled. This can thus be activated in production.


