import { Component } from '@angular/core';
import {ApiHelperService} from './app.apihelper.service';

@Component({
  selector: 'ad-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(private apiHelper: ApiHelperService) { }
  logout() {
    this.apiHelper.logout();
  }
}
