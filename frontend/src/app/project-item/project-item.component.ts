import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ApiHelperService } from '../app.apihelper.service';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-project-item',
  templateUrl: './project-item.component.html'
})
export class ProjectItemComponent implements OnInit {
  @Input() project;
  @Output("getAllProjects") getAllProjects: EventEmitter<any> = new EventEmitter();
  constructor(private apiHelper: ApiHelperService, private notifyService: NotificationService) { }
  ngOnInit(): void {

  }

  deleteProject(pid) {
    this.apiHelper.deleteProject(pid).subscribe(
      (res) => {
        this.notifyService.showSuccess("Project Deleted Successfully", "");
        this.getAllProjects.emit();
      },
      (err) => {
        this.notifyService.showError("Oops Error!!", err);
      }
    );
  }
}
