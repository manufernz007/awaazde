import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ApiHelperService } from '../app.apihelper.service';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '../notification.service'

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html'
})
export class TaskDetailComponent implements OnInit {
  public form: FormGroup;
  public pid;
  public tid;
  public task;
  public userList;
  constructor(private apiHelper: ApiHelperService, private activatedRoute: ActivatedRoute, private notifyService: NotificationService) { }

  getTaskById(id) {
    this.apiHelper.getTaskById(id).subscribe(data => {
      console.log(data);
      this.task = data["results"][0];
      this.form = new FormGroup({
        name: new FormControl(this.task.name),
        description: new FormControl(this.task.description),
        start: new FormControl(this.task.start_date),
        end: new FormControl(this.task.end_date),
        user: new FormControl(this.task.user)
      });
    });
  }
  getAllUsers() {
    this.apiHelper.getAllUsers().subscribe(data => {
      console.log(data);
      this.userList = data["results"];


    });
  }

  ngOnInit(): void {
    this.pid = this.activatedRoute.snapshot.paramMap.get('pid')
    this.tid = this.activatedRoute.snapshot.paramMap.get('tid')
    this.getTaskById(this.tid);
    this.getAllUsers();
  }

  updateTask(task): void {
    let formData = new FormData();
    formData.append('name', task.name);
    formData.append('description', task.description);
    formData.append('start_date', task.start);
    formData.append('end_date', task.end);
    formData.append('user', task.user);
    formData.append('project', this.pid);
    this.apiHelper.updateTask(formData, this.tid).subscribe(
      (res) => {
        this.notifyService.showSuccess("Task Update Successful", "");
      },
      (err) => {
        this.notifyService.showError("Oops Error!!", err);
      }
    );
  }

}
