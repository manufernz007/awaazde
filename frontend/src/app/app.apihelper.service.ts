import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {  Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class ApiHelperService {
 
  // http options used for making API calls
  private httpOptions: any;
 
  // the actual JWT token
  public token: string;
 
  // the token expiration date
  public token_expires: Date;
 
  // the username of the logged in user
  public username: string;
 
  // error messages received from the login attempt
  public errors: any = [];
 
  public BASE_URL='http://ec2-13-233-140-28.ap-south-1.compute.amazonaws.com/';
  constructor(private http: HttpClient,private router: Router ) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
  }
 

  public login(user) {
    this.http.post(this.BASE_URL+'api-token-auth/', JSON.stringify(user), this.httpOptions).subscribe(
      data => {
        localStorage.setItem('token', data['token']);
        this.router.navigate(['/projects']);
        location.reload();
      },
      err => {
        this.errors = err['error'];
      }
    );
  }
 public getAllProjects():Observable<any>{
    let httpOptions = {
        headers: new HttpHeaders({
          'Authorization': 'Token ' +localStorage.getItem('token')
        })
      };
    return this.http.get(this.BASE_URL+'api/projects', httpOptions);
 }

 public getAllUsers():Observable<any>{
  let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' +localStorage.getItem('token')
      })
    };
  return this.http.get(this.BASE_URL+'api/users', httpOptions);
}
 

 public getProjectById(id):Observable<any>{
  let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' +localStorage.getItem('token')
      })
    };
  return this.http.get(this.BASE_URL+'api/projects/?id='+id, httpOptions);
}

 public addProject(project):Observable<any>{
    let httpOptions = {
        headers: new HttpHeaders({
          'Authorization': 'Token ' +localStorage.getItem('token')
        })
      };
    return this.http.post(this.BASE_URL+'api/project/new', project,httpOptions);
 }
 public updateProject(project,pid):Observable<any>{
  let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' +localStorage.getItem('token')
      })
    };
  return this.http.put(this.BASE_URL+'api/project/'+pid+'/update', project,httpOptions);
}
 public deleteProject(pid):Observable<any>{
  let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' +localStorage.getItem('token')
      })
    };
  return this.http.delete(this.BASE_URL+'api/project/'+pid+'/destroy',httpOptions);
}



public getAllTasks():Observable<any>{
  let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' +localStorage.getItem('token')
      })
    };
  return this.http.get(this.BASE_URL+'api/tasks', httpOptions);
}
public getTaskById(id):Observable<any>{
  let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' +localStorage.getItem('token')
      })
    };
  return this.http.get(this.BASE_URL+'api/tasks/?id='+id, httpOptions);
}


public addTask(project):Observable<any>{
  let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' +localStorage.getItem('token')
      })
    };
  return this.http.post(this.BASE_URL+'api/task/new', project,httpOptions);
}

public updateTask(task,tid):Observable<any>{
  let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' +localStorage.getItem('token')
      })
    };
  return this.http.put(this.BASE_URL+'api/task/'+tid+'/update', task,httpOptions);
}

public deleteTask(tid):Observable<any>{
let httpOptions = {
    headers: new HttpHeaders({
      'Authorization': 'Token ' +localStorage.getItem('token')
    })
  };
return this.http.delete(this.BASE_URL+'api/task/'+tid+'/destroy',httpOptions);
}
  public isLoggedIn():boolean {
    if(localStorage.getItem('token')=='' || localStorage.getItem('token')===null ){
        this.router.navigate(['']);
        return false;
        }else{
        
          return true;
        }
  }
  public isGuest():boolean {
    if(localStorage.getItem('token')=='' || localStorage.getItem('token')===null ){
       
        return true;
        }else{
            this.router.navigate(['/projects']);
          return false;
        }
  }
  
  public logout() {
    localStorage.setItem('token', '');
    this.token = null;
    this.token_expires = null;
    this.username = null;
    this.router.navigate(['']);
    location.reload();
  }
 

 
}