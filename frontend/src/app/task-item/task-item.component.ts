import { Component, OnInit,Input,EventEmitter,Output } from '@angular/core';
import {ApiHelperService} from '../app.apihelper.service';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html'
})
export class TaskItemComponent implements OnInit {
  @Input() task;
  @Input() pid;
  @Output("getAllTasks") getAllTasks: EventEmitter<any> = new EventEmitter();
  constructor(private apiHelper: ApiHelperService, private notifyService: NotificationService) { }

  ngOnInit(): void {
  }
  deleteTask(pid){
    this.apiHelper.deleteTask(pid).subscribe(
      (res) => {
        this.notifyService.showSuccess("Task Deleted Successfully", "");
        this.getAllTasks.emit();
      },
      (err) => {  
        this.notifyService.showError("Oops Error!!", err);
      }
    );
  }
}
