import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ApiHelperService } from '../app.apihelper.service';
import { NotificationService } from '../notification.service';
@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
})
export class ProjectListComponent implements OnInit {
  form: FormGroup;
  projectList;
  showAddProject: boolean;
  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('image').setValue(file);
    }
  }

  constructor(private apiHelper: ApiHelperService, private notifyService: NotificationService) {
    this.showAddProject = false;
  }

  getAllProjects() {
    this.apiHelper.getAllProjects().subscribe(data => {
      console.log(data);
      this.projectList = data["results"];
    });
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(''),
      description: new FormControl(''),
      duration: new FormControl(''),

      image: new FormControl('')
    });
    this.getAllProjects();
  }

  addProject(project): void {
    let formData = new FormData();
    formData.append('name', project.name);
    formData.append('description', project.description);
    formData.append('duration', project.duration);
    formData.append('image', this.form.get('image').value);
    this.apiHelper.addProject(formData).subscribe(
      (res) => {
        this.notifyService.showSuccess("Project Added Successfully", "");
        this.getAllProjects();
      },
      (err) => {
        this.notifyService.showError("Oops Error!!", err);
      }
    );
  }
}
