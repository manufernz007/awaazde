import { Component, OnInit } from '@angular/core';
import { ApiHelperService } from '../app.apihelper.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  isAuthenticated: boolean;
  constructor(private apiHelper: ApiHelperService) { }

  ngOnInit(): void {
    this.isAuthenticated = this.apiHelper.isLoggedIn();
  }

  logout() {
    this.apiHelper.logout();
  }
}
