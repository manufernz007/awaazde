import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ApiHelperService } from '../app.apihelper.service';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html'
})
export class ProjectDetailComponent implements OnInit {
  public form: FormGroup;
  public editform: FormGroup;
  public taskList;
  public showAddTask: boolean;
  public pid;
  public project;
  public userList;
  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.editform.get('image').setValue(file);
    }
  }
  constructor(private apiHelper: ApiHelperService, private activatedRoute: ActivatedRoute, private notifyService: NotificationService) {
    this.showAddTask = false;
  }

  getProjectById(id) {
    this.apiHelper.getProjectById(id).subscribe(data => {
      console.log(data);
      this.project = data["results"][0];
      this.editform = new FormGroup({
        name: new FormControl(this.project.name),
        description: new FormControl(this.project.description),
        duration: new FormControl(this.project.duration),
        image: new FormControl('')
      });

    });
  }
  getAllTasks() {
    this.apiHelper.getAllTasks().subscribe(data => {
      console.log(data);
      this.taskList = data["results"];

    });
  }

  getAllUsers() {
    this.apiHelper.getAllUsers().subscribe(data => {
      console.log(data);
      this.userList = data["results"];

    });
  }
  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(''),
      description: new FormControl(''),
      end: new FormControl(''),
      start: new FormControl(''),
      user: new FormControl('')
    });

    this.getAllTasks();
    this.getAllUsers();
    this.pid = this.activatedRoute.snapshot.paramMap.get('pid')
    this.getProjectById(this.pid);
  }

  addTask(task): void {
    console.log(task)
    let formData = new FormData();
    formData.append('name', task.name);
    formData.append('description', task.description);
    formData.append('start_date', task.start);
    formData.append('end_date', task.end);
    formData.append('project', this.pid);
    formData.append('user', task.user);
    this.apiHelper.addTask(formData).subscribe(
      (res) => {
        this.notifyService.showSuccess("Task Added Successfully", "");
        this.getAllTasks();
      },
      (err) => {
        this.notifyService.showError("Oops Error!!", err);
      }
    );
  }

  updateProject(project): void {
    let formData = new FormData();
    formData.append('name', project.name);
    formData.append('description', project.description);
    formData.append('duration', project.duration);
    if (this.editform.get('image').value != null && this.editform.get('image').value != '') {
      formData.append('image', this.editform.get('image').value);
    }

    this.apiHelper.updateProject(formData, this.pid).subscribe(
      (res) => {
        this.notifyService.showSuccess("Project Updated Successfully", "");
        location.reload();
      },
      (err) => {
        this.notifyService.showError("Oops Error!!", err);
      }
    );
  }
}
