import { Component, OnInit } from '@angular/core';
import { ApiHelperService } from '../app.apihelper.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  constructor(private apiHelper: ApiHelperService) { }

  ngOnInit(): void {
  }
  login(i) {
    this.apiHelper.login({ 'username': i.username, 'password': i.password });
  }

}
