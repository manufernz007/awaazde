import { Routes,RouterModule } from '@angular/router';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard }  from './auth.guard';
import { GuestGuard }  from './guest.guard';

const appRoutes:Routes=[
{path:'',component:HomeComponent,canActivate: [GuestGuard]  },
{path:'projects',component:ProjectListComponent, canActivate: [AuthGuard]},
{path:'project/:pid',component:ProjectDetailComponent , canActivate: [AuthGuard]},
{path:'project/:pid/task/:tid',component:TaskDetailComponent , canActivate: [AuthGuard]},



];

export const Routing=RouterModule.forRoot(appRoutes, { useHash: true });